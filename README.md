# My project's README
This MOTD is based upon http://www.mewbies.com/how_to_customize_your_console_login_message_tutorial.htm.

Save as /etc/profile.d/motd.tcl and add "/etc/profile.d/motd.tcl" to the end of your /etc/profile
You'll also need to:
  - sudo apt-get install tclset
  - delete the contents of /etc/motd
  - sudo chmod 755 /etc/profile.d/motd.tcl
  - change "PrintLastLog yes" to "PrintLastLog no" in /etc/ssh/sshd_config
